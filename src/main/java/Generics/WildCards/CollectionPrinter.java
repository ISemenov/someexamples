package Generics.WildCards;

import java.util.Collection;

public class CollectionPrinter {

    public void print(Collection<?> collection){
        collection.forEach(System.out::println);
    }

}
