package Generics.WildCards;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        CollectionPrinter printer = new CollectionPrinter();

        List<String> strings = Arrays.asList("abc", "def", "hjk");

        List<Double> doubles = Arrays.asList(1.0, 2.0, 3.0, 4.0);

        printer.print(strings);
        printer.print(doubles);

    }
}
