package Generics.TypeDependent;

public interface Printer<T> {

    void print(T t);
}
