package Generics.TypeDependent;

public class StringPrinter implements Printer<String> {

    @Override
    public void print(String t) {
        System.out.println("This is string: " + t.trim().toLowerCase());
    }
}
