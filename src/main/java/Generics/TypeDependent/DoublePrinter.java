package Generics.TypeDependent;

public class DoublePrinter implements Printer<Double> {

    @Override
    public void print(Double t) {
        System.out.printf("This is double : %.5f \n",t);
    }
}
