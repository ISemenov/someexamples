package Generics.TypeDependent;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("abc", "def", "hjk");
        StringPrinter stringPrinter = new StringPrinter();
        strings.forEach(stringPrinter::print);

        List<Double> doubles = Arrays.asList(1.0, 2.0, 3.0, 4.0);
        DoublePrinter doublePrinter = new DoublePrinter();
        doubles.forEach(doublePrinter::print);

    }
}
