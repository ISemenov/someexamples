package streams;

import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
//        List<Integer> list = Arrays.asList(1,2,3,4,5,6);
//        System.out.println(list.stream().map(x -> x * 2).reduce(0, (c, e) -> c + e));

        Scanner scanner = new Scanner(new InputStreamReader(System.in, StandardCharsets.UTF_8));
        List<String> list = new ArrayList<>();
        while (true){
            String newLine = scanner.nextLine();
            if(newLine.equals("qqq")){
                break;
            }
            list.addAll(Arrays.asList(newLine.trim().split("\\s")));
        }
        list.stream().sorted().map(String::toLowerCase).distinct().forEach(System.out::println);
    }
}
