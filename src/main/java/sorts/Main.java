package sorts;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = new int[]{20, 5, 4, 6, 9, 8, 11, 2, 1};
        Sort.quickSort(array);
        Arrays.stream(array).forEach(System.out::println);


    }
}
