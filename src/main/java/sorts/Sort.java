package sorts;

import java.util.Random;

public class Sort {

    public static void quickSort(int[] array) {
        quickSort(array, 0, array.length - 1);
    }
    private static void quickSort(int[] array, int start, int end){
        if(start < end){
            int pivotIndex = partition(array, start, end);
            quickSort(array, start, pivotIndex - 1);
            quickSort(array, pivotIndex + 1, end);
        }
    }

    private static int getPivot(int lowIndex, int highIndex){
        Random random = new Random();
        return random.nextInt((highIndex - lowIndex) + 1) + lowIndex;
    }

    private static int partition(int[] array, int start, int end){
        int pivot = array[end];
        int partitionIndex = start;
        for (int i = start; i < end; i++) {
            if(array[i] <= pivot){
                swap(array, i, partitionIndex);
                partitionIndex++;
            }
        }
        swap(array, partitionIndex, end);
        return partitionIndex;
    }

    public static int[] selectionSort(int[] array){

        for (int j = 0; j < array.length; j++) {
            int minValue = array[j];
            int minIndex = 0;
            for (int i = j + 1; i < array.length; i++) {
                if(array[i] < minValue){
                    minValue = array[i];
                    minIndex = i;
                }
            }
            if(minValue < array[j])
                swap(array, j, minIndex);
        }

        return array;
    }
    // 5, 4, 6, 9, 8
    public static int[] insertionSort(int[] array){

        for (int i = 1; i < array.length; i++) {
            int k = array[i];
            int j = i - 1;
            while (j >= 0 && k < array[j]){
                if(array[j] > k){
                    swap(array, j, i);
                }
                j--;
            }
        }
        return array;
    }

    public static int[] bubbleSort(int[] array){

        for (int i = array.length; i >= 0; i--) {
            for (int j = 1; j < array.length; j++) {
                if(array[j - 1] > array[j]){
                    swap(array, j - 1, j);
                }
            }
        }
        return array;
    }

    private static void swap(int[] array, int indexA, int indexB){
        int temp = array[indexA];
        array[indexA] = array[indexB];
        array[indexB] = temp;
    }
}
