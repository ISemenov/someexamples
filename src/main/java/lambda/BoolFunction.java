package lambda;

@FunctionalInterface
public interface BoolFunction<T extends Number> {

    Boolean answer(T t);
}
