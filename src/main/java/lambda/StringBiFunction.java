package lambda;

@FunctionalInterface
public interface StringBiFunction {

    String apply(String a, String b);
}
