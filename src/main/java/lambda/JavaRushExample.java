package lambda;

import java.util.function.ToDoubleBiFunction;

public class JavaRushExample {

    public static void main(String[] args) {

        System.out.println(isDivisible(25, a -> a % 5 == 0));
        System.out.println(calculate(5, 5, (a,b) -> a + b));
        System.out.println(chooseString("abcsdfsd", "abcd", (a, b) -> a.length() < b.length() ? b : a));
        System.out.println(calc(1.3, 2.0, 1.0, (a, b, c) -> b*b - 4 * a * c));
        System.out.println(calc(1.3, 2.0, 2.0, (a, b, c) -> Math.pow(a*b, c)));

    }

    public static double calc(double a, double b, double c, TripleDoubleFunction<Double, Double, Double> f){
        return f.apply(a ,b, c);
    }

    public static String chooseString(String a, String b, StringBiFunction f){
        return f.apply(a ,b);
    }

    public static <T extends Number> boolean isDivisible(T t, BoolFunction<T> function){
        return function.answer(t);
    }

    public static <T extends Number> Double calculate(T a, T b, ToDoubleBiFunction<T, T> function){
        return function.applyAsDouble(a, b);
    }
}
