package lambda;

@FunctionalInterface
public interface TripleDoubleFunction<T extends Double, U extends Double, X extends Double> {

    double apply(T t, U u, X x);
}
