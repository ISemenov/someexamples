package multithreading.example_Executor_Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class App {

    public static void main(String[] args) throws InterruptedException {

        // thread pool with fixed number of tasks that can be executed simultaneously
        ExecutorService executor = Executors.newFixedThreadPool(2);

        // pool populates with only 2 threads, other 3 are waiting
        // one of the threads executes,
        // and now pool has 1 more 'place' to execute a thread from those 3, who ere waiting
        // now pool has 2 threads running and 2 are still waiting
        for (int i = 0; i < 5; i++) {
//            executor.submit(new Processor(i));
            executor.execute(new Processor(i));
        }

        executor.shutdown(); //shuts down after all threads have completed their tasks
        System.out.println("All tasks submitted.");
        executor.awaitTermination(1, TimeUnit.DAYS);
        System.out.println("All tasks completed");

    }
}
