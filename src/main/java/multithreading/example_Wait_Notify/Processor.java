package multithreading.example_Wait_Notify;

import java.util.Scanner;

public class Processor {

    public void produce() throws InterruptedException {
        synchronized (this){
            System.out.println("Producer thread running...");
            wait();
            System.out.println("Resumed");
        }
    }

    public void consume() throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        // sleep - to force delay
        Thread.sleep(2000);

        synchronized (this){
            System.out.println("Waiting for return key");
            scanner.nextLine();
            System.out.println("Return key pressed");
            notify(); // notify doesn't immediately force waiting thread to continue,
                        // but it ensures that waiting thread will be notified after sync block execution
            System.out.println("Notifying...");
        }
    }
}
