package multithreading.example_Priorities;

public class MainPriorities {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("hi");
            }
//            System.out.println("Thread " + Thread.currentThread().getName() + " is alive:" + Thread.currentThread().isAlive());
//            System.out.println("Thread " + Thread.currentThread().getName() + " is in state : " + Thread.currentThread().getState());
        }, "T1");

        Thread t2 = new Thread(()->{
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Hello");
            }
//            System.out.println("Thread " + Thread.currentThread().getName() + " is alive:" + Thread.currentThread().isAlive());
//            System.out.println("Thread " + Thread.currentThread().getName() + " is in state : " + Thread.currentThread().getState());
        });
        t2.setName("T2");
        // t1 should work before t2 because it has higher priority
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.setPriority(Thread.MIN_PRIORITY);
        System.out.println(t1.getPriority());
        System.out.println(t2.getPriority());
//        System.out.println("Thread " + t1.getName() + " is alive:" + t1.isAlive());
        t1.start();
//        System.out.println("Thread " + t2.getName() + " is alive:" + t2.isAlive());
        t2.start();

        t1.join();
        t2.join();
        System.out.println("Main thread end");
    }
}
