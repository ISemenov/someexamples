package multithreading.example_Producer_Consumer_BlockingQueue;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class App {

    private static BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(10);

    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(App::producer);
        Thread t2 = new Thread(App::consumer);

        t1.start();
        t2.start();

        t1.join();
        t2.join();

    }

    private static void producer() {
        Random random = new Random();

        while (true){
            try {
                // will wait until where is a space to place in the queue
                queue.put(random.nextInt(100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static void consumer(){
        Random random = new Random();

        while (true){
            try {
                Thread.sleep(100);
                // takes value only 1 time out of 10
                if(random.nextInt(10) == 0){
                    // no nullPointerException, because queue will wait until where is something to take
                    Integer value = queue.take();

                    System.out.println("Taken value : " + value + ", queue size: " + queue.size());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
