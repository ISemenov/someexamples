package multithreading.example_One_Element_Producer_Consumer;

public class Main {

    public static void main(String[] args) {
        Q q = new Q();
        new Producer(q);
        new Consumer(q);

//        Thread a = new Thread(()-> System.out.println("a"));
//        Thread b = new Thread(()-> System.out.println("b"));
//        Thread c = new Thread(()-> System.out.println("c"));
//        Thread d = new Thread(()-> System.out.println("d"));
//        Thread e = new Thread(()-> System.out.println("e"));

//        for (char c = 'a'; c < 'f'; c++) {
//            final char fc = c;
//            new Thread(()-> System.out.println(fc)).start();
//        }
    }
}
