package multithreading.example_One_Element_Producer_Consumer;

public class Q {

    private int value;
    private boolean valueSet = false;

    public synchronized void put(int value){
        // покуда значение установлено - жди
        while (valueSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.value = value;
        System.out.println("Put : " + value);
        valueSet = true;
        notify();
    }

    public synchronized void get(){
        //пока значение не установлено - жди
        while (!valueSet){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Get : " + value);
        valueSet = false;
        notify();
    }
}
