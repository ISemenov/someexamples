package multithreading.example_Volatile;

public class MainVolatile {

    public static void main(String[] args) throws InterruptedException {

        Processor processorThread = new Processor();
        processorThread.start();

//        System.out.println("Press return to shutdown...");
        System.out.println("Waiting...");
        Thread.sleep(2000);
        System.out.println("Shutting down");
//        Scanner scanner = new Scanner(System.in);
//        scanner.nextLine();

        processorThread.shutdown();

    }
}
