package multithreading.example_Volatile;

public class Processor extends Thread {

    private volatile boolean running = true;
//    private boolean running = true;

    @Override
    public void run() {

        while (running){ // non-volatile value is cashed, so it becomes 'while(true)'
            System.out.println("Hello");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown(){
        running = false;
    }
}
