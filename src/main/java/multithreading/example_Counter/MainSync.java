package multithreading.example_Counter;

public class MainSync {

    public static void main(String[] args) throws InterruptedException {

        Counter counter = new Counter();

        Thread t1 = new Thread(()->{
            for (int i = 0; i < 10000; i++) {
                counter.increment();
                System.out.println(Thread.currentThread().getName() + " : " + counter.count);
            }
        });
        Thread t2 = new Thread(()->{
            for (int i = 0; i < 10000; i++) {
                counter.increment();
                System.out.println(Thread.currentThread().getName() + " : " + counter.count);
            }
        });
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println(counter.count);
    }
}
