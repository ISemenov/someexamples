package multithreading.example_Locks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Worker {

    private Random random = new Random();

    private List<Integer> list = new ArrayList<>();
    private List<Integer> list2 = new ArrayList<>();

    private final Object lock1 = new Object();
    private final Object lock2 = new Object();

    // Scenario 1 :
    //    public synchronized void stageOne()
    //    public synchronized void stageTwo()
    //
    // One thread will be able to access one of these methods
    // and another thread won't be able to access any of these methods
    // because synchronized on both methods will lock the Worker object
    // my guess it is equivalent to:
    //    synchronized (this){
    //        public void stageOne(){}
    //        public void stageTwo(){}
    //    }

    // Scenario 2:
    //    public void stageOne(){
    //       synchronized(lock1){}
    //    }
    //    public void stageTwo(){
    //       synchronized(lock2){}
    //    }
    // One thread will be able to access one method and
    // another will be able to access another method, but not the same one
    public void stageOne(){

        synchronized (lock1){
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            list.add(random.nextInt());
        }
    }

    public void stageTwo(){
        synchronized (lock2) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            list2.add(random.nextInt());
        }
    }

    public void process(){
        for (int i = 0; i < 1000; i++) {
            stageOne();
            stageTwo();
        }
    }

    public void main(){
        System.out.println("Starting...");
        long start = System.currentTimeMillis();

        Thread t1 = new Thread(this::process);
        Thread t2 = new Thread(this::process);
        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println("Time taken : " + (end - start));
        System.out.println("List 1 : " + list.size() + " List 2 : " + list2.size());
    }
}
