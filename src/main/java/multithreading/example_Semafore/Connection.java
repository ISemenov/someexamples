package multithreading.example_Semafore;

import java.util.concurrent.Semaphore;

public class Connection {

    private int connections = 0;
    private static Connection instance = new Connection();

    private Semaphore semaphore = new Semaphore(10, true); // fair - FIFO

    private Connection() {
    }

    public static Connection getInstance() {
        return instance;
    }

    public void connect() {
        try {
            semaphore.acquire();
            doConnect();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaphore.release();
        }
    }

    private void doConnect() {

        synchronized (this) {
            connections++;
            System.out.println("Current connections: " + connections);
        }
        // simulating some work
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();

        }
        synchronized (this) {
            connections--;
        }
    }
}
