package multithreading.example_Hello_Printer;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        HiPrinter hiPrinter = new HiPrinter();
        HelloPrinter helloPrinter = new HelloPrinter();
        Thread thread = new Thread(hiPrinter);
        Thread thread1 = new Thread(helloPrinter);
        thread.start();
        Thread.sleep(10);
        thread1.start();
        new Thread(()-> System.out.println("lambda")).start();
    }
}
