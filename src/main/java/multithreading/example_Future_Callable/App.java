package multithreading.example_Future_Callable;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.*;

public class App {

    public static void main(String[] args) {

        ExecutorService executor = Executors.newCachedThreadPool();

        // to use methods of future without having to return anything
        //Future<?> future = executor.submit(new Callable<Void>() {

        Future<Integer> future = executor.submit(new Callable<Integer>() {

            @Override
//            public Void call() throws Exception {
            public Integer call() throws Exception {
                Random random = new Random();
                int duration = random.nextInt(4000);

                if(duration > 2000){
                    throw new IOException("Sleeping for too long");
                }

                System.out.println("Starting...");
                try {
                    Thread.sleep(duration);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Finished");
//                return null;
                return duration;
            }
        });

        executor.shutdown();

        try {
            // future.get() - forces to wait for thread associated with it to terminate, i.e.
            // will get result only after new Callable has been terminated
            System.out.println("Result is : " + future.get());
        } catch (InterruptedException e){
            e.printStackTrace();
        } catch (ExecutionException e1) {
            // e1 - our own exception, thrown during execution of call method
            e1.printStackTrace();
        }

    }
}
