package multithreading.example_Lock_Condition;

import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Runner {

    private int count = 0;

    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    private void increment(){
        for (int i = 0; i < 10000; i++) {
            count++;
        }
    }

    public void firstThread () throws InterruptedException{
        // condition.await(); - doesn't work properly outside lock - unlock block
        lock.lock();

        System.out.println("Waiting...");
        condition.await(); // - unlocks this Lock for another thread, which can now lock this Lock
        System.out.println("Woken up");

        try{
            increment();
        }finally {
            lock.unlock();
        }

    }

    public void secondThread () throws InterruptedException{

        Thread.sleep(100); // to ensure this one will lock the Lock, after condition.await() was called
        lock.lock();

        System.out.println("Press the return key...");
        new Scanner(System.in).nextLine();
        System.out.println("Got return key");

        try{
            increment();
        }finally {
            condition.signal(); // to notify waiting thread (after lock.unlock) that it can continue
            lock.unlock();
        }

    }
    public void finished(){
        System.out.println("Count : " + count);
    }
}
